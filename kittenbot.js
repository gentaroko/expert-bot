/* *****************************************************************************
Copyright 2016 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License")
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
********************************************************************************

This is a sample Slack bot built with Botkit.
*/

var Botkit = require('botkit')
var util = require('util')
var fs = require('fs')
const BigQuery = require('@google-cloud/bigquery');
var keyword_extractor = require("keyword-extractor");

var controller = Botkit.slackbot({debug: false, interactive_replies: true})

if (!process.env.slack_token_path) {
  console.log('Error: Specify slack_token_path in environment')
  process.exit(1)
}

fs.readFile(process.env.slack_token_path, function (err, data) {
  if (err) {
    console.log('Error: Specify token in slack_token_path file')
    process.exit(1)
  }
  data = String(data)
  data = data.replace(/\s/g, '')
  controller
    .spawn({token: data})
    .startRTM(function (err) {
      if (err) {
        throw new Error(err)
      }
    })
})

// The project ID to use, e.g. "your-project-id"
const projectId = "grtest123456789";

// Instantiates a client
const bigquery = BigQuery({
  projectId: projectId
});

function createQuery (action, platform) {
  // Extract the keywords
  var extraction_result = keyword_extractor.extract(action,{
                                                                  language:"english",
                                                                  remove_digits: false,
                                                                  return_changed_case:true,
                                                                  remove_duplicates: true
                                                             });

  var actionKeywords = extraction_result.join(' ') ;

  var sqlQuery = `CREATE TEMPORARY FUNCTION getQuestionLink(Id FLOAT64)
RETURNS STRING
 LANGUAGE js AS """
 return "https://stackoverflow.com/questions/" + Id;
""";

CREATE TEMPORARY FUNCTION questionRank(title STRING, body STRING, tags STRING, phrase STRING)
RETURNS FLOAT64
 LANGUAGE js AS """
 var keys = phrase.split(" ");
 var score = 0;
 if (title.includes(phrase))
 score +=100;
 for (i = 0; i < keys.length; i++) {
  if (title.includes(keys[i]))
  score +=2;
  if(body.includes(keys[i]) || tags.includes(keys[i]))
  score +=1;
 }
 return score;
""";

CREATE TEMPORARY FUNCTION answerRank(Id FLOAT64)
RETURNS FLOAT64
 LANGUAGE js AS """
 if (Id == null)
 return 0.0;
 else
 return 1.0;
""";

CREATE TEMPORARY FUNCTION scoreRank(answerId FLOAT64)
RETURNS FLOAT64
 LANGUAGE js AS """
 if (answerId == null)
 return 0.0;
 else
 return 1.0;
""";

SELECT title, getQuestionLink(q.id) as link, accepted_answer_id, answerRank(accepted_answer_id) as arank, a.score as ascore, questionRank(q.title, q.body, q.tags,"`;

    sqlQuery += actionKeywords;
    sqlQuery += `") as qrank
FROM \`bigquery-public-data.stackoverflow.posts_questions\` q LEFT JOIN \`bigquery-public-data.stackoverflow.posts_answers\` a
ON q.accepted_answer_id = a.id
WHERE q.tags like '%`

    sqlQuery += platform.toLowerCase();;

    sqlQuery += `%'`

    sqlQuery += ` ORDER BY qrank DESC, arank DESC, ascore DESC
LIMIT 5`;

  return sqlQuery;
}

function callBigQueryApi (sqlQuery) {
  var options = {
      query: sqlQuery,
      useLegacySql: false // Use standard SQL syntax for queries.
    };

  return new Promise((resolve, reject) => {
    bigquery
    .query(options)
    .then((results) => {
      const rows = results[0];
       // Resolve the promise with the output text
       resolve(rows);
    })
    .catch((err) => {
      reject(err);
    });
  });
}

function formatBigQueryOutput (bigQueryOutput) {
  var output = "You will find the following links helpful to your question:\n\n"
  var counter = 0
  for(var item of bigQueryOutput) {
    output += util.format('Question: %s, %s\n', item.title, item.link);
   }

  return output;
}

controller.hears(
  ['aws', 'AWS', 'azure', 'Azure', 'gcp', 'GCP'],
  ['ambient','direct_message', 'direct_mention', 'mention'],
  function (bot, message) {
    var action = message.text.substring(10, message.text.indexOf("on")-1);
    var platform = message.text.substring(message.text.indexOf("on")+3, message.text.length-1);

    callBigQueryApi(createQuery(action,platform)).then((output) => {
      // Return the results of the weather API to API.AI
      console.log('Output: ' + output);
      var response = formatBigQueryOutput(output);
      console.log('Response: ' + response);
      bot.reply(message, response)
    }).catch((err) => {
      console.log('Error: ' + err);
      // If there is an error let the user know
      bot.reply(message, 'There\'s been an error on your request. Please try again.')
    });
    bot.reply(message, 'This might take a while...')
  }
);
