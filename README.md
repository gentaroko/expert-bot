# README #

This README would normally document whatever steps are necessary to get your application up and running.


### What is this repository for? ###

.create(v=3.0) internal hackathon for creating a service that answers to cloud realted questions


### Who do I talk to? ###

Alvaro Mongil (alvaro.mongil@cloudreach.com)

Genta Roko (genta.roko@cloudreach.com)


### What does this application do? ###

This is a slack bot which answers technical questions by querying the Public Datasets like Stack Overflow, Github, etc


### Which components have been used? ###

 -> Slack Application: ExpertBot
 
 -> Google Cloud Platform: Google Containers, Google BigQuery


### What is the workflow of this application? ###


1.  Human asks MadExpertBot in the format: "How do I"  + <Query> + "in" + <Cloud Platform>

2.  Bot sends query to  GCP Function via Slack Bot App  by using the Access token to authorise BotKit in GCP

3.  Function Separates Platform  and Query elements

4.  Function constructs the Query in BigQuery format (requesting: ID, rank and score)

5.  Run BQ query

6.  Get json result from the BQ run

7.  Process the query result file, print  answer title and create original post link by using the ID from the  result file

8.  Create the reply string in the BotKit

9.  BotKit sends to Mad Expert Bot

10. Result is displayed in the slack chat window.


### How are the results fetched? ###


-> prepare the Public Dataset  query by atomising the keywords in the question

-> remove the unnecessary words from it, like: in, it, at, for, with etc.

-> With the remaining keywords in the question create the SQL-like query for the GCP BigQuery engine

-> push the query and get a json file as result, containing all the hits which my query had

-> use the IDs of the Stackoverflow questions in the result to retrieve the Title and the Link

-> stream the results in slack to the mortal human who asked the question in the first place


### What could be added in the future? ###


-> improve my language skills and manners

-> improve my interpretation skills

-> send prettier results to the human (send previews)

-> optimise the query cost by caching or selecting more accurrate keywords

-> ask for clarification in case of too many results

-> allow the human to get the next results, not only the top 5

-> improve the ranking of the questions and answers

-> Query other datasets like past experts group emails, previous searches etc
 
 